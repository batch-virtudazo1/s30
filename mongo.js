// db.fruits.insertMany([
// 
// 		{
// 			name: "Apple",
// 			supplier: "Red Farms Inc.",
// 			stocks: 20,
// 			price: 40,
// 			onSale: true
// 		},
// 		{
// 			name: "Banana",
// 			supplier: "Yellow Farms",
// 			stocks: 15,
// 			price: 20,
// 			onSale: true
// 		},
// 		{
// 			name: "Kiwi",
// 			supplier: "Green Farming and Canning",
// 			stocks: 25,
// 			price: 50,
// 			onSale: true
// 		},
// 		{
// 			name: "Mango",
// 			supplier: "Yellow Farms",
// 			stocks: 10,
// 			price: 60,
// 			onSale: true
// 		},
// 		{
// 			name: "Dragon Fruit",
// 			supplier: "Red Farms Inc.",
// 			stocks: 10,
// 			price: 60,
// 			onSale: true
// 		},
// 
// 
// 	])

// Aggregation 
// it has also 2 to 3 steps recoomended.
// each process in aggregation is called stage.
// $match it like a query so you can use query operators here like $regex
//  {$match:{field: <value>}}

// $group allows us to group together documents.
//  _id: points out the name of the specific field on a document
db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier", totalStocks: {$sum:"$stocks"}}}

])
        // null/_id will create one group and makes it add all the sum of stocks    
db.fruits.aggregate([
    
        {$match: {onSale:true}},
        {$group: {_id:null, totalStocks: {$sum:"$stocks"}}}
    
])
        
db.fruits.aggregate([
    
        {$match: {onSale:true}},
        {$group: {_id:"AllOnSaleFruits", totalStocks: {$sum:"$stocks"}}}
    
])


        
db.fruits.aggregate([
    
        {$match: {supplier: "Red Farms Inc."}},
        {$group: {_id:"RedFarms", totalStocks: {$sum:"$stocks"}}}
    
])


//  Mini Activity
db.fruits.aggregate([
    
        {$match: {supplier: "Yellow Farms", onSale: true }},
        {$group: {_id:"Yellow Farms On Sale", total: {$sum:"$stocks"}}}
    
])
        
// $avg - is an operator used in $group and gets the avg of the numerical value of the indicated field
db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group:{_id:"$supplier", avgStock: {$avg: "$stocks"}}}
        
]) 
       
db.fruits.aggregate([
        
        {$match:{onSale: true}},
//         null/other name besides the field name talaga means one group only
        {$group:{_id:null,avgPrice: {$avg:"$price"}}}
        
 ])        
        
// $max - allows to get the highest value out of all the given value in given fields in $group
db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group: {_id: "highestStockOnSale", maxStock: {$max: "$stocks"}}}
            
])  
       
db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group: {_id: null, maxPrice: {$max: "$price"}}}
            
]) 
        
// $min - gets the lowest value naman.
        
db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group: {_id: "lowestStockOnSale", minStock: {$min: "$stocks"}}}
            
])
        
db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$group: {_id: "lowestPriceOnSale", minPrice: {$min: "$price"}}}
            
])
        
// Mini Activity       
db.fruits.aggregate([
        
        {$match:{price: {$lt:50}}},
        {$group: {_id: null, minStock: {$min: "$stocks"}}}
            
])
        
// $count it is added after the $match and count all the items
        
db.fruits.aggregate([
        
        {$match:{onSale:true}},
        {$count: "itemsOnSale"}
            
])
db.fruits.aggregate([
        
        {$match:{price:{$lt:50}}},
        {$count: "itemsPriceLessThan50"}
            
])
        
db.fruits.aggregate([
        
        {$match:{stocks:{$lt:20}}},
        {$count: "forRestock"}
            
])
        
// $out allows us to SAVE/OUTPUT the result in the new collection.
// it will OVERWRITE the collection if it is already existing.
        
db.fruits.aggregate([
        
        {$match:{onSale: true}},
        {$group: {_id:"$supplier", totalStocks: {$sum: "$stocks"}}},
        {$out: "stockPerSupplier"}
])