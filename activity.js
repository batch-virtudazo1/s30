db.fruits.aggregate([


        {$match:{supplier: "Yellow Farms", price:{$lt:50}}},
        {$group:{_id:"$supplier", totalItems: {$sum: "$stocks"}}},
        {$count: "priceLessThan50"}
])


// db.fruits.aggregate([
// 
//         {$match:{price:{$lt:30}}},
//         {$group: {_id:null}},
//         {$count: "priceLessThan30"}
// ])

// db.fruits.aggregate([
//     
//     {$match: {supplier: "Yellow Farms"}},
//     {$group:{_id:"$supplier", avgPrice: {$avg: "$price"}}}
// 
// ])

// db.fruits.aggregate([
//     
//     {$match: {supplier: "Red Farms Inc."}},
//     {$group:{_id:"$supplier", highestPrice: {$max: "$price"}}}
// 
// ])
// 
// db.fruits.aggregate([
//     
//     {$match: {supplier: "Red Farms Inc."}},
//     {$group:{_id:"$supplier", lowestPrice: {$min: "$price"}}}
// 
// ])

